package com.example.dosactivitysbidireccionales;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    public static final String NOMBRE="nom", EDAD="edad", TELEFONO="tel";
    private TextView tvNom, tvTel, tvEdad;
    public static  final int REQUEST_CODE=1234;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        cargarViews();
    }
    //--------------------------------------------------------------------------------------
    public void cargarViews(){
        tvNom=(TextView)findViewById(R.id.tvNom);
        tvTel=(TextView)findViewById(R.id.tvTele);
        tvEdad=(TextView)findViewById(R.id.tvEdad);
    }
    //----------------------------------------------------------------------------------------
    public void editar(View v){
        Intent i = new Intent(this, DatosActivity.class);
        Bundle datos = new Bundle();
        if(!tvNom.getText().toString().trim().isEmpty()){
            datos.putString(NOMBRE, tvNom.getText().toString().trim());
        }
        if(!tvTel.getText().toString().trim().isEmpty()){
            datos.putString(TELEFONO, tvTel.getText().toString().trim());
        }
        if(!tvEdad.getText().toString().trim().isEmpty()){
            datos.putInt(EDAD, Integer.parseInt(tvEdad.getText().toString().trim()));
        }
        i.putExtras(datos);
        startActivityForResult(i, REQUEST_CODE);
    }
    //---------------------------------------------------------------------------------------------

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==REQUEST_CODE && resultCode==RESULT_OK){
            //la cosa ha ido bien
            //recojemos los datos que vienen del activity datos
            if(data.hasExtra(NOMBRE)){
                tvNom.setText(data.getStringExtra(NOMBRE));
            }
            if(data.hasExtra(TELEFONO)){
                tvTel.setText(data.getStringExtra(TELEFONO));
            }
            if(data.hasExtra(EDAD)){
                tvEdad.setText(""+data.getIntExtra(EDAD, -1));
            }
        }
        else if(requestCode==REQUEST_CODE && resultCode==RESULT_CANCELED){
            Toast.makeText(this, getResources().getString(R.string.usuarioBack), Toast.LENGTH_SHORT).show();
        }

    }
    //-----------------------------------------------------------------------------------------
}