package com.example.dosactivitysbidireccionales;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class DatosActivity extends AppCompatActivity {
    private EditText etNombre, etTelefono;
    private TextView tvProgreso;
    private SeekBar sbEdad;
    private int edad;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_datos);
        cargarViews();
        //para que aparezca la seek bar en 18
        edad=18;
        tvProgreso.setText(String.format(getResources().getString(R.string.tvSeek), edad));
        ponerListener();
        cargarDatos();
    }
    //----------------------------------------------------------------------------------------------
    public void cargarDatos(){
        Bundle datos=getIntent().getExtras();

        //Compruebo sim me han ido llegando los campos
        if(getIntent().hasExtra(MainActivity.NOMBRE)){
            //Me ha llegado el nombre del activity anterior
            etNombre.setText(datos.getString(MainActivity.NOMBRE));
        }
        if(getIntent().hasExtra(MainActivity.TELEFONO)){
            //Me llegó el telefono del activity anterior
            etTelefono.setText(datos.getString(MainActivity.TELEFONO));
        }
        if(getIntent().hasExtra(MainActivity.EDAD)){
            //Me ha llegado el dato edad del activity anterior
            int p=datos.getInt(MainActivity.EDAD, -1);
            sbEdad.setProgress(p-18);
            tvProgreso.setText(String.format(getResources().getString(R.string.tvSeek), p));
        }
    }
    //----------------------------------------------------------------------------------------------
    public void volver(View v){
        Intent i = new Intent();
        //compruebo que lo campos no están vacios y los mando
        if(etNombre.getText().toString().trim().length()!=0){
            i.putExtra(MainActivity.NOMBRE, etNombre.getText().toString().trim());
        }
        if(etTelefono.getText().toString().trim().length()!=0){
            i.putExtra(MainActivity.TELEFONO, etTelefono.getText().toString().trim());
        }
        //la seek bar estará a 18 o al valor que hayamos puesto
        i.putExtra(MainActivity.EDAD, edad);
        setResult(RESULT_OK, i);
        finish();
    }

    //----------------------------------------------------------------------------------------------
    public void cargarViews(){
        etNombre=(EditText)findViewById(R.id.etNombre);
        etTelefono=(EditText)findViewById(R.id.etTelefono);
        tvProgreso=(TextView)findViewById(R.id.tvSeek);
        sbEdad=(SeekBar)findViewById(R.id.sbEdad);
    }
    //----------------------------------------------------------------------------------------------
    public void ponerListener(){
        sbEdad.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                edad=18+progress;
                tvProgreso.setText(String.format(getResources().getString(R.string.tvSeek), edad));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }
    //----------------------------------------------------------------------------------------------
}
